from django.test import TestCase

from .forms import RegistrationForm


class RegistrationForm(TestCase):
def test_form_has_fields(self):
form = SignUpForm()
expected = ['username', 'name', 'email', 'password1', 'password2',]
actual = list(form.fields)
self.assertSequenceEqual(expected, actual)