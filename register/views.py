from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import RegistrationForm
from django.core.mail import send_mail
from store.models import Customer, User
# get information to send mails
from ecommerce.settings import ADMIN_EMAIL, CONTACT_EMAIL


# register the new user / customer
def register(response):
	# if one uses the form
	if response.method == "POST":
		form = RegistrationForm(response.POST)
		# GreenOasis has to check whether the form is valid
		if form.is_valid():
			user = form.save() # creates user
			Customer.objects.create(
				user = user,
				name = user.username,
				email = user.email )    
			# Since we send "real emails, we will send them to our shop's email address for now
			send_mail("Green Oasis Account", "Welcome to GreenOasis", CONTACT_EMAIL, ADMIN_EMAIL, fail_silently=False,)
			# User doesn't create Customer automatically, but those are OneToOneFields, thus both of them are needed
			# users = User.objects.all()
			# for user in users:
			# 	Customer.objects.get_or_create(user=user)
				# Once the new user is created, one can login 
		return redirect("/login")
	else:
		# If the form isn't valid, one will receive error messages. Otheriwse nothing will happen
		form = RegistrationForm()
	return render(response, "register/signup.html", {"form":form})
