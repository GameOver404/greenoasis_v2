from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from .models import *
from django.core import serializers
import json
import markdown2
import bleach

def questions(request):
    context = {}
    # get all questions
    context['questions'] = Question.objects.all()
    return render(request, 'questions_home.html', context)

def askquestion(request):
    if request.method == 'POST':
        try:
            # user needs to give title, question and his/her name
            title = request.POST.get('title')
            question = request.POST.get('question')
            posted_by = request.POST.get('posted_by')
            # The question item is then created in order to save it
            q = Question(question_title=title, question_text=question, posted_by=posted_by)
            q.save()
            # One is then redirected to the detailed view of one's question
            return redirect(viewquestion, q.qid, q.slug)
            # we have to catch exceptions, i.e. one potentially forogt to give a titel
        except Exception as e:
            return render(request, 'ask.html', { 'error': 'Something is wrong with the form!' })
    else:
        # If the method is not post, we will go back / stay at the same page
        return render(request, 'ask.html', {})

# Of course we can view questions in detail as well
def viewquestion(request, qid, qslug):
    context = {}
    # We have to get the specific question to show it (as well as the answers/responses)
    question = Question.objects.get(qid=qid, slug=qslug)

    # AJAX is used and the data is serialized
    question_json = json.loads(serializers.serialize('json', [ question ]))[0]['fields']
    # We have to get the information as JSON-data
    question_json['date_posted'] = question.date_posted
    question_json['qid'] = question.qid
    # Bleach ensures that no XSS attacks can occur, as everything which is not on the whitelist, is not accepted / processed
    question_json['question_text'] = bleach.clean(markdown2.markdown(question_json['question_text']), tags=['p', 'pre','code', 'sup', 'strong', 'hr', 'sub', 'a'])
    context['question'] = question_json
    # To get the answers one has to "speak" with the answer model
    context['answers'] = []
    answers = Answer.objects.filter(qid=qid)
    # Every answer has to be checked with bleach - only the tags in the whitelist are allowed
    # This is better than a blacklist, as we cannot forget a potentially dangerous command
    for answer in answers:
        answer.answer_text = bleach.clean(markdown2.markdown(answer.answer_text), tags=['p', 'pre','code', 'sup', 'strong', 'hr', 'sub', 'a'])
        # We retrieve all the answers for the specific question
        context['answers'].append(answer)
        # and show all the data to the user
    return render(request, 'view_question.html', context)

# csrf_exempt is a class decorator for excluding cross site request forgery, as it is done with AJAX and whitelist
@csrf_exempt
def ajaxanswerquestion(request):
    if request.method == 'POST':
        try:
            # The data needs to be in the JSON format
            json_data = json.loads(request.body)
            # The answer and the name of the poster are needed
            answer = json_data['answer']
            posted_by = json_data['posted_by']
            # and the questions has a unique (q)id.
            qid = json_data['qid']
            # Create a Answer model
            answer = Answer(answer_text=answer, posted_by=posted_by, qid=Question.objects.get(qid=qid))
            # and save it to the database
            answer.save()
            # Once the data is saved, we can send a JSON-response
            return JsonResponse({'Success': 'Answer posted successfully.'})
            # Of course, exceptions can occur
        except Exception as e:
            # We print the exception to the console
            print(e)
            # and return a JSON response
            return JsonResponse({'Error': 'Something went wrong when posting your answer.'})