from django.contrib import admin
from .models import *

# Question and answer are saved in the DB and can be adjusted by the admin
admin.site.register(Question)
admin.site.register(Answer)
