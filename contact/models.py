from django.db import models

# The three attributes email, subject and message are needed
class Contact(models.Model):
    email = models.EmailField()
    # written in German and with capital letters, since it is shown that way in the webshop
    subject = models.CharField(max_length=255)
    message = models.TextField()

    def __str__(self):
        return self.email