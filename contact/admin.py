from django.contrib import admin
from .models import Contact
# The contact messages are saved and can be seen (and e.g. deleted) by every admin
admin.site.register(Contact)