from django.forms import ModelForm
from .models import Contact

# All fields of the model contact will need to be filled out by the user
class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'