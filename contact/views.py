# from django.shortcuts import render
# from .forms import ContactForm


# def contact_view(request):
#     if request.method == 'POST':
#         form = ContactForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return render(request, 'contact/success.html')
#     form = ContactForm()
#     context = {'form': form}
#     return render(request, 'contact/contact.html', context)

from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .forms import ContactForm


def contact_view(request):
    # Form needs to be filled out for actions to take place
    if request.method == 'POST':
        form = ContactForm(request.POST)
        # Initially one needs to check whether the form is valid, e.g. could it really be an email address in the field email?
        if form.is_valid():
            # Saves the answers from the form to the database
            form.save()
            # Writes a simple email to the admin (GreenOasis.Pflanzen@gmail.com)
            email_subject = f'New contact {form.cleaned_data["email"]}: {form.cleaned_data["subject"]}'
            email_message = form.cleaned_data['message']
            send_mail(email_subject, email_message, settings.CONTACT_EMAIL, settings.ADMIN_EMAIL)
            # User is redirected to inform him/her about the successful mail delivery 
            return render(request, 'contact/success.html')
    form = ContactForm()
    context = {'form': form}
    return render(request, 'contact/contact.html', context)