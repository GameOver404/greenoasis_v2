from django.urls import path
from . import views

urlpatterns = [
    path('', views.allblogs, name='allblogs'),
    # Just some added community games
    path('games.html', views.games, name='games'), 
    path('<int:post_id>', views.blog_post, name='blog_post')
]