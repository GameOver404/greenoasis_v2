from django.contrib import admin

# Register your models here.

from .models import BlogEntry
# admin needs to add new blog entries
admin.site.register(BlogEntry)
