from django.db import models
from django.utils import timezone

# Create your models here.

# Create blog entries for the blogs with the following attributes:
class BlogEntry(models.Model):
    title = models.CharField(max_length=100)
    pub_date = models.DateTimeField(verbose_name='Publication Date')
    image = models.ImageField(upload_to='')
    body = models.TextField()

    def __str__(self):
        return self.title
    
# The publication date is printed in a specified format, since it looks better than the standard format
    def pub_date_pretty(self):
        self.pub_date = self.pub_date.strftime("%d %b %Y")
        return self.pub_date

# If the blogpost is too long, one solely shows a part of the text followed by three dots
    def summary(self):
        if len(self.body.split()) <= 100:
            return self.body
        else:
            words = self.body.split()
            word_list = []
            i = 0
            while i < 100:
                word_list += words[i]+" "
                i += 1
            words = "".join(word_list)+" ..."
            return words 

