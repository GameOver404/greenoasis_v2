from django.shortcuts import render, get_object_or_404
from . import models

# Create your views here.
def allblogs(request):
    blogentries = models.BlogEntry.objects
    return render(request, 'allblogs.html', {"blogs":blogentries})

def blog_post(request, post_id):
    single_post = get_object_or_404(models.BlogEntry, pk=post_id)
    return render(request, 'blogpost.html', {'single_blog':single_post})

def games(request):
    return render(request, 'games.html')