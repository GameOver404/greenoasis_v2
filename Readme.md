# GreenOasis - MW210 Webshop

This webshop was created with Django and Bootstrap + MDB (Material Design for Bootstrap)

## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip  

## Dependencies

* Django
* Django-Allauth
* Django-Countries
* Django-Crispy-Forms

To install all the needed dependencies run:

```
pip install -r requirements.txt
```

## Usage

* Once the dependencies are installed (via pip) start the app with:

```
python manage.py runserver
```

# Gifs
## Übersicht und Warenkorb

![](gifs/Produkt_hinzufügen_und_Warenkorb_anzeigen.gif "Übersicht")


### Statische Seiten

![](gifs/Statische_Seiten.gif)

## Blog + Spieleseite

![](gifs/Blog+Spieleseite.gif)
