"""
Django settings for ecommerce project.

Generated by 'django-admin startproject' using Django 3.0.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os
from dotenv import load_dotenv
load_dotenv()
from decouple import config

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# We kept all the data worthy of protection a "secret". Well, it can be found in the .env file, but it would
# NOT be pushed to Git under normal circumstances. You'll find it there, since it is needed to run the application properly.
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Due to security reasons the allowed hosts must be specified 
ALLOWED_HOSTS = ['127.0.0.1']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.sites',
    'about',
    'contact',
    'blog',
    'questions',
    'newsletter',
    'register',
    'coupon_management',

    'crispy_forms',

    'store.apps.StoreConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ecommerce.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

SITE_ID=1

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]

DSC_COUPON_CODE_LENGTH = 15

# This is effectively a minimum value
# A salt of 128 would be an entropy of 131 bits.
salt_entropy = 168

# Checks whether the reviews contain profanities and if so, they aren't published
REVIEW_ALLOW_PROFANITIES = False

# CRISPY FORMS

CRISPY_TEMPLATE_PACK = 'bootstrap4'

# Security measures
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'

# One requires a certificate, when one wants to start the localhost with http secure
if DEBUG == False:
    SECURE_SSL_REDIRECT = True
    SESSION_COOKIE_SECURE = True
    SESSION_COOKIE_SAMESITE = None

SESSION_COOKIE_DOMAIN = '127.0.0.1'
CSRF_COOKIE_SECURE = True

# PayPal Sandbox
PAYPAL_SANDBOX_MODE = True
PAYPAL_CURRENCY = 'EUR'
PAYPAL_BRAND_NAME = 'GreenOasis'

# PayPal sandbox user
PAYPAL_API_USERNAME = os.environ.get('PAYPAL_API_USERNAME')
PAYPAL_API_PASSWORD = os.environ.get('PAYPAL_API_PASSWORD')
PAYPAL_API_SIGNATURE = os.environ.get('PAYPAL_API_SIGNATURE')

# Login / Logout

# LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

# Payment with Stripe (bank / credit card)
# Can be found in the .env file

# Email is printed to the console, intead of sent via SMTP
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

CONTACT_EMAIL = 'GreenOasis.Pflanzen@gmail.com'
ADMIN_EMAIL = ['GreenOasis.Pflanzen@gmail.com', ]

# Twilio SendGrid
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'apikey'
EMAIL_HOST_PASSWORD = os.environ.get('SENDGRID_API_KEY')

WSGI_APPLICATION = 'ecommerce.wsgi.application'

GOOGLE_ANALYTICS_ID = os.environ.get('GOOGLE_ANALYTICS_ID')
# Secure method "to take dumps" (sorry for the word play)
# Despite the dump function, it of course is used to serialize 
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Toggle sandbox mode (when running in DEBUG mode) - currently set to false, as no mails are sent to unkown users
SENDGRID_SANDBOX_MODE_IN_DEBUG=False

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
           'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

# The webshop sends to  Germany and Austria and thus the chosen language is German.
LANGUAGE_CODE = 'de'
# Of course we'll take the Central European Time
TIME_ZONE = 'CET'
# In case we'd like to offer the webshop in other languages (e.g. English), we need this information
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Checks whether the reviews contain profanities and if so, they aren't published
REVIEW_ALLOW_PROFANITIES = False

# CRISPY FORMS
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]

MEDIA_URL = '/images/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'static/images')