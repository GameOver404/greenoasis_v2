from django.shortcuts import render
from django.views.generic.base import View

def about_view(request):
    return render(request, 'about/about_us.html')

def privacy_view(request):
    return render(request, 'about/privacy_policy.html')

def policy_view(request):
    return render(request, 'about/shipping_policy.html')

def terms_view(request):
    return render(request, 'about/terms_of_service.html')

def summary_view(request):
    return render(request, 'about/summary.html')
