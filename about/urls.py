from django.conf.urls import url
from django.urls import path
from django.views.generic import TemplateView
from . import views


urlpatterns = [
    path('about_us.html', views.about_view, name='about_us'),
    path('terms_of_service.html', views.terms_view, name='terms_of_service'),
    path('privacy_policy.html', views.privacy_view, name='privacy_policy'),
    path('shipping_policy.html', views.policy_view, name='shipping_policy'),
    path('summary.html', views.summary_view, name='summary'),
]
