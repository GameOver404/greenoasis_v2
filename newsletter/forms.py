from django import forms

class SubscriberForm(forms.Form):
    email = forms.EmailField(label='GreenOasis.Pflanzen@gmail.com',
                             max_length=100,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))