from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Subscriber
from ecommerce.settings import CONTACT_EMAIL
from .forms import SubscriberForm
@csrf_exempt
def new(request):
    form = SubscriberForm()
    if request.method == 'POST':
        if form.is_valid:
            sub = Subscriber(email=request.POST['email'])
            sub.save()
            return render(request, 'footer.html', {'email': sub.email, 'action': 'added', 'form': SubscriberForm()})
        else: 
            pass
    else:
        pass

def confirm(request):
    sub = Subscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.confirmed = True
        sub.save()
        return render(request, 'footer.html', {'email': sub.email, 'action': 'confirmed'})
    else:
        return render(request, 'footer.html', {'email': sub.email, 'action': 'denied'})

def delete(request):
    sub = Subscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.delete()
        return render(request, 'footer.html', {'email': sub.email, 'action': 'unsubscribed'})
    else:
        return render(request, 'footer.html', {'email': sub.email, 'action': 'denied'})

