from django.shortcuts import render
from django.http import JsonResponse
import json
import datetime
from .models import * 
from .utils import cookieCart, cartData, guestOrder
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic import View, UpdateView
from django.utils.decorators import method_decorator

def category_selection(request):
	product = Product.objects.all()
	categories = []
	for p in product:
		if product.category not in categories:
			categories.append(product.category)
	context = {'cat': categories}
	print(context)
	return render(request, 'store/navbar.html', context)

def store(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	products = Product.objects.all()
	context = {'products':products, 'cartItems':cartItems}
	return render(request, 'store/store.html', context)

def logout(request):
	auth.logout(request)
	messages.info(request, "Sie haben sich abgemeldet")
	return redirect('store')

def profile(request):
	if request.user.is_authenticated:
		return render(request, 'accounts/profile.html')
	else:
		return redirect('registration/login/')
		
from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.core.mail import send_mail

def cart(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/cart.html', context)
		
def checkout(request):
	data = cartData(request)
	
	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/checkout.html', context)

def updateItem(request):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	print('Action:', action)
	print('Product:', productId)

	customer = request.user.customer
	product = Product.objects.get(id=productId)
	order, created = Order.objects.get_or_create(customer=customer, complete=False)
	orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

	if action == 'add':
		orderItem.quantity = (orderItem.quantity + 1)
	elif action == 'remove':
		orderItem.quantity = (orderItem.quantity - 1)

	orderItem.save()

	if orderItem.quantity <= 0:
		orderItem.delete()

	return JsonResponse('Item was added', safe=False)

# def addRating(request,id):
#     product = get_object_or_404(Product, pk=id)
#     pro = Product.objects.get(id=id)
#     if request.method == "POST":
#         form = RatingForm(request.POST)
#         if form.is_valid():
#             product = form.cleaned_data['product']
#             user = form.cleaned_data['user']
#             rating = form.cleaned_data['rating']

#             product = request.POST.get('product', ''),
#             user = request.POST.get('user', ''),
#             rating = request.POST.get('rating', ''),

#             obj = Rating(product=product, user=user, rating=rating)
#             obj.save()
#             context = {'obj': obj}
#             return render(request, 'store.html',context)
#         else:
#            form=RatingForm()
#         return HttpResponse('Geben Sie eine Bewertung ab')

def processOrder(request):
	transaction_id = datetime.datetime.now().timestamp()
	data = json.loads(request.body)

	if request.user.is_authenticated:
		customer = request.user.customer
		order, created = Order.objects.get_or_create(customer=customer, complete=False)
	else:
		customer, order = guestOrder(request, data)

	total = float(data['form']['total'])
	order.transaction_id = transaction_id

	if total == order.get_cart_total:
		order.complete = True
	order.save()

	if order.shipping == True:
		ShippingAddress.objects.create(
		customer=customer,
		order=order,
		address=data['shipping']['address'],
		city=data['shipping']['city'],
		state=data['shipping']['state'],
		zipcode=data['shipping']['zipcode'],
		)

	return JsonResponse('Bezahlung übermittelt!', safe=False)

	def get_random_item(self):
		items = list(Product.objects.all())
		random_item = random.choice(items)
		return random_item

@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    model = User
    fields = ('name','email', )
    template_name = 'account.html'
    success_url = reverse_lazy('my_account')

    def get_object(self):
        return self.request.user

class ProductAddView(View):
        @login_required
        def add_to_wishlist(request,slug):

            item = get_object_or_404(Item,slug=slug)

            wished_item,created = Wishlist.objects.get_or_create(wished_item=item,
            slug = item.slug,
            user = request.user,
            )

            messages.info(request,'Das Produkt wurde zu Ihrer Merkliste hinzugefügt.')
            return redirect('product_detail',slug=slug)

