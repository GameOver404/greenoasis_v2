from django.urls import path, include
from django.conf.urls import url
from . import views

urlpatterns = [
	#Leave as empty string for base url
	path('', views.store, name="store"),
	path('cart/', views.cart, name="cart"),
	path('checkout/', views.checkout, name="checkout"),

	path('update_item/', views.updateItem, name="update_item"),
	path('process_order/', views.processOrder, name="process_order"),
	path('', include('django.contrib.auth.urls')),
	path('accounts/profile/', views.profile, name='profile'),
	path('category_selection/', views.category_selection, name="category_selection"),


	# url(r'^details/edit_details/(?P<user_id>\d+)/$', views.edit_details, name='edit_details'),
    # url(r'^details/edit_address/(?P<user_id>\d+)/$', views.edit_address, name='edit_address'),
]