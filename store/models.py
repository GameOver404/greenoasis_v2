from django import forms
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser, UserManager

# Create your models here.

class Customer(models.Model):
	user=models.OneToOneField(
    User,
    null=True,
    blank=True,
    on_delete=models.CASCADE,
    related_name="customer"
)
	name = models.CharField(max_length=200, null=True)
	email = models.CharField(max_length=200)

	def __str__(self):
		return self.name

class User(AbstractBaseUser):
	objects =  UserManager()

CATEGORY_CHOICES = (
    ('I', 'Zimmerpflanzen'),
    ('O', 'Outdoor-Pflanzen'),
    ('S', 'Samen'),
    ('Z', 'Zubehör')
)

RATING_CHOICES =(
    ("1", "Schlecht"),
    ("2", "Akzeptabel"),
    ("3", "In Ordnung"),
    ("4", "Sehr gut"),
    ("5", "Perfekt"),
)

class Product(models.Model):
	name = models.CharField(max_length=200)
	price = models.IntegerField()
	category = models.CharField(choices=CATEGORY_CHOICES, max_length=2)
	discount_price = models.FloatField(blank=True, null=True)
	description = models.TextField()
	digital = models.BooleanField(default=False,null=True, blank=True)
	image = models.ImageField(null=True, blank=True)

	def __str__(self):
		return self.name

	@property
	def imageURL(self):
		try:
			url = self.image.url
		except:
			url = ''
		return url

class Order(models.Model):
	customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
	date_ordered = models.DateTimeField(auto_now_add=True)
	complete = models.BooleanField(default=False)
	transaction_id = models.CharField(max_length=100, null=True)

	def __str__(self):
		return str(self.id)
		
	@property
	def shipping(self):
		shipping = False
		orderitems = self.orderitem_set.all()
		for i in orderitems:
			if i.product.digital == False:
				shipping = True
		return shipping

	@property
	def get_cart_total(self):
		orderitems = self.orderitem_set.all()
		total = sum([item.get_total for item in orderitems])
		return total 

	@property
	def get_cart_items(self):
		orderitems = self.orderitem_set.all()
		total = sum([item.quantity for item in orderitems])
		return total 



class Wishlist(models.Model):
    user = models.ForeignKey(Customer,on_delete=models.PROTECT)
	# here CASCADE is the behavior to adopt when the referenced object(because it is a foreign key) is deleted. it is not specific to django,this is an sql standard.
    wished_item = models.ForeignKey(Product,on_delete=models.CASCADE)
    slug = models.CharField(max_length=30,null=True,blank=True)
    added_date = models.DateTimeField(auto_now_add=True)

def __str__(self):
    return self.wished_item.title

class Refund(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    reason = models.TextField()
    accepted = models.BooleanField(default=False)
    email = models.EmailField()

    def __str__(self):
        return f"{self.pk}"

class OrderItem(models.Model):
	product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
	order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
	quantity = models.IntegerField(default=0, null=True, blank=True)
	date_added = models.DateTimeField(auto_now_add=True)

	@property
	def get_total(self):
		total = self.product.price * self.quantity
		return total

class Rating(models.Model):
    product=models.ForeignKey(Product,default=None, on_delete=models.PROTECT)
    user=models.ForeignKey(Customer,default=None, on_delete=models.PROTECT)
    rating = forms.ChoiceField(choices = RATING_CHOICES)

class ShippingAddress(models.Model):
	customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
	order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
	address = models.CharField(max_length=200, null=False)
	city = models.CharField(max_length=200, null=False)
	state = models.CharField(max_length=200, null=False)
	zipcode = models.CharField(max_length=200, null=False)
	date_added = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.address