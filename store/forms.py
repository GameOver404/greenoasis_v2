from django import forms

class PaymentForm(forms.Form):
    # If one pays with Stripe this token is needed, otherwise one doesn't need it
    stripeToken = forms.CharField(required=False)
    # The credit card data isn't stored by our webshop, as it is more secure to let Stripe or PayPal handle it
    save = forms.BooleanField(required=False)
    use_default = forms.BooleanField(required=False)

